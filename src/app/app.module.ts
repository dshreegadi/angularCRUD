import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { AddModalComponent } from './Shared/Component/add-modal/add-modal.component';
import { InlineEditComponent } from './Shared/Component/inline-edit/inline-edit.component';
import { DataTableComponent } from './Shared/Component/data-table/data-table.component';
import { StudentService } from '../app/Shared/Services/student.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule} from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    AddModalComponent,
    InlineEditComponent,
    DataTableComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [StudentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
