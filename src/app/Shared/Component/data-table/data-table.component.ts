import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { StudentService } from '../../Services/student.service';
import { AddModalComponent } from '../add-modal/add-modal.component';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent implements OnInit {
  @Output() displayModal: any;
  // @Input() values: any;
  Id: number;
  // For Json
  student: any;
  columns: any;
  data: any;

  // For checkbox
  checkedItems: any;
  checkedItemCount: any;
  isCheckSingleItem: any;
  isCheckAllItems: any;

  isShowIcon: any;

  iconUrl: string;

  constructor(private studentService: StudentService) {
    this.Id = 1;

    this.student = [];
    this.columns = [];
    this.data = [];

    this.checkedItems = [];
    this.checkedItemCount = 0;
    this.isCheckSingleItem = false;
    this.isCheckAllItems = false;

    this.isShowIcon = false;
    this.iconUrl = '../../../../assets/Icons/editIcon.png';
  }

  ngOnInit() {
    this.studentService.getStudents()
      .subscribe(studentData => {
        this.columns = studentData[0];
        this.student = studentData[1];
        for (let i = 1; i < this.student.length; i++) {
          this.Id = i;
        }
      });
  }

  addStudent() {
    this.displayModal = true;
  }

  deleteStudent() {
    // console.log('in delete' + this.checkedItems.length);
    for (let i = this.checkedItems.length - 1; i >= 0; i--) {
      if (this.checkedItems[i]) {
        this.student.splice(i, 1);
        // console.log(i);
        this.checkedItemCount = 0;
      }
    }
    this.checkedItems = [];
  }

  checkSingle(ev, i) {
    if (ev.target.checked) {
      this.checkedItems[i] = true;
      this.checkedItemCount++;
      // console.log('Count =' + this.checkedItemCount);

      if (this.checkedItemCount === this.student.length) {
        this.isCheckAllItems = true;
      }
    } else {
      this.isCheckAllItems = false;
      this.checkedItemCount--;
      // console.log('Count =' + this.checkedItemCount);
    }
  }

  checkAll(ev) {
    console.log(ev.target.value);
    this.isCheckSingleItem = true;
    if (ev.target.checked) {
      for (let i = 0; i < this.student.length; i++) {
        this.checkedItems[i] = true;
        this.isCheckSingleItem = true;
      }
      this.checkedItemCount = this.student.length;
      // console.log('checked all:' + this.checkedItemCount);
      this.isCheckAllItems = true;
    } else {
      this.isCheckSingleItem = false;
      this.isCheckAllItems = false;
      for (let i = 0; i < this.student.length; i++) {
        this.checkedItems[i] = false;
        this.checkedItemCount = 0;
      }
    }
  }

  cancelModal = function ($event) {
    this.displayModal = false;
  };

  receiveValues($event) {
    this.student.unshift($event);
  }

  // values = function ($event) {

  //   console.log($event);
  //   // console.log(this.values.value);
  //   this.student.unshift(this.$event.value);
  // };
}
