import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, FormControlName } from '@angular/forms';
import { StudentService } from '../../Services/student.service';

@Component({
  selector: 'app-add-modal',
  templateUrl: './add-modal.component.html',
  styleUrls: ['./add-modal.component.css']
})
export class AddModalComponent implements OnInit {
  @Input() displayChild: any;
  @Output()
  hideModal: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  sendFormValues: EventEmitter<any> = new EventEmitter<any>();

  student: any;
  studentData: any;

  userForm: FormGroup;

  constructor(private fb: FormBuilder,
    private studentService: StudentService) {
    this.userForm = this.fb.group({
      // Id: new FormControl(null, [Validators.required]),
      Firstname: new FormControl(null, [Validators.required]),
      Lastname: new FormControl(null, [Validators.required]),
      Age: new FormControl(null, [Validators.required]),
      Gender: new FormControl(null, [Validators.required]),
      Schoolname: new FormControl(null, [Validators.required]),
      Collegename: new FormControl(null, [Validators.required]),
      DOB: new FormControl(null, [Validators.required])
    });
    this.student = [];
    this.studentData = [];
  }

  ngOnInit() {
    this.studentService.getStudents()
      .subscribe(studentData => {
        this.student = studentData[1];
      });
  }

  onSubmit() {
    // if (this.userForm.valid) {
      console.log(this.userForm.value);
      this.sendFormValues.emit(this.userForm.value);
      this.userForm.reset();
    // }
  }

  showModal() {
    this.displayChild = true;
  }

  cancel($event) {
    this.displayChild = false;
    this.hideModal.emit($event);
  }

}
