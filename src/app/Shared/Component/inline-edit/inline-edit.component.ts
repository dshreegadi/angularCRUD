import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inline-edit',
  templateUrl: './inline-edit.component.html',
  styleUrls: ['./inline-edit.component.css']
})
export class InlineEditComponent implements OnInit {

  isShowIcon: boolean;
  constructor() {
    this.isShowIcon = false;
  }

  ngOnInit() {
  }

  mouseenter(ev) {
    this.isShowIcon = true;
  }

  mouseleave(ev) {
    this.isShowIcon = false;
  }
}
